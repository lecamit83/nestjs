import { Injectable, NotFoundException } from '@nestjs/common';

import { CreateTaskDTO } from './dto/create-task.dto';
// import { GetFilterTaskDTO } from './dto/get-tasks-filter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskRepository } from './task.repository';
import { Task } from './task.entity';
import { TaskStatus } from './task-status.enum';
import { DeleteResult } from 'typeorm';
import { GetFilterTaskDTO } from './dto/get-tasks-filter.dto';

@Injectable()
export class TasksService {
  constructor(@InjectRepository(TaskRepository) private taskRepository : TaskRepository) {}
  // private tasks : Task[] = [];

  getTasks(filterTaskDTO: GetFilterTaskDTO) : Promise<Task[]> {
    return this.taskRepository.getTasks(filterTaskDTO);
  }

  // getFilterTasks(filterTaskDTO : GetFilterTaskDTO) : Task[] {
  //   const { status, search } = filterTaskDTO;
  //   let tasks = this.getTasks();
  //   if(status) {
  //     tasks = tasks.filter(task => task.status === status);
  //   }
  //   if(search) {
  //     tasks = tasks.filter(task => (task.title.includes(search) || task.description.includes(search)));
  //   }
  //   return this.tasks;
  // }

  async createTask(createTaskDTO : CreateTaskDTO) : Promise<Task> {
    return this.taskRepository.createTask(createTaskDTO);
  }

  async getTaskById(id : number) : Promise<Task> {
    const task = await this.taskRepository.findOne(id);
    if(!task) {
      throw new NotFoundException(`Task with ID=${id} not found.`);
    }
    return task;
  }

  async deleteTaskById(id: number) : Promise<void> {
    const result: DeleteResult = await this.taskRepository.delete(id);
    if(result.affected === 0) {
      throw new NotFoundException(`Task with ID=${id} not found.`);
    }
  }

  async updateStatusById(id: number, status : TaskStatus) : Promise<Task> {
    const task = await this.getTaskById(id);
    task.status = status;
    await task.save();
    return task;
  }
}
