import { Repository, EntityRepository } from "typeorm";
import { Task } from "./task.entity";
import { CreateTaskDTO } from "./dto/create-task.dto";
import { TaskStatus } from "./task-status.enum";
import { GetFilterTaskDTO } from "./dto/get-tasks-filter.dto";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {
  async createTask(createTaskDTO: CreateTaskDTO): Promise<Task> {
    const { title, description } = createTaskDTO;

    const task: Task = new Task();
    task.title = title;
    task.description = description;
    task.status = TaskStatus.OPEN;
    await task.save();

    return task;
  }

  async getTasks(filterTaskDTO: GetFilterTaskDTO): Promise<Task[]> {
    const { status, search } = filterTaskDTO;
    let query = this.createQueryBuilder('task');

    if (status) {
      query.andWhere("task.status = :status", { status });
    }

    if (search) {
      query.andWhere("task.title LIKE :search OR task.description LIKE :search", { search: `%${search}%` });
    }

    const tasks = query.getMany();

    return tasks;
  }
}