import { Controller, Get, Post, Body, Param, Delete, Patch, Query, UsePipes, ValidationPipe, ParseIntPipe } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { Task } from './task.entity';
import { CreateTaskDTO } from './dto/create-task.dto';
import { GetFilterTaskDTO } from './dto/get-tasks-filter.dto';
import { TaskStatusValidationPipe } from './pipes/task-status-validator.pipe';
import { TaskStatus } from './task-status.enum';

@Controller('tasks')
export class TasksController {
  constructor(private tasksService: TasksService) {}

  @Get()
  getTasks(@Query(ValidationPipe) filterTaskDTO: GetFilterTaskDTO) : Promise<Task[]> {
    return this.tasksService.getTasks(filterTaskDTO);
  }

  @Post()
  @UsePipes(ValidationPipe)
  createTask(@Body() createTaskDTO : CreateTaskDTO) : Promise<Task> {
    return this.tasksService.createTask(createTaskDTO);
  }

  @Get("/:id")
  getTaskById(@Param("id", ParseIntPipe) id: number) : Promise<Task> { 
    return this.tasksService.getTaskById(id);
  }

  @Delete("/:id")
  deleteTaskById(@Param("id", ParseIntPipe) id: number) : Promise<void> {
    return this.tasksService.deleteTaskById(id);
  }

  @Patch("/:id/status")
  updateStatusById(@Param("id", ParseIntPipe) id: number, @Body("status", TaskStatusValidationPipe) status: TaskStatus) : Promise<Task> {
    return this.tasksService.updateStatusById(id, status);
  }
}
