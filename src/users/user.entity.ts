import { Entity, Column, BaseEntity, Unique, PrimaryGeneratedColumn } from "typeorm"

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id : string;

  @Column()
  email : string;

  @Column()
  password : string;
}