import { IsNotEmpty, IsString, MinLength, MaxLength, Matches } from "class-validator";

export class AuthCredentialsDTO {
  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(50)
  email : string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(50)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {message : 'Password too weaks'})
  password : string;
}