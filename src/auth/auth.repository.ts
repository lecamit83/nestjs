import { Repository, EntityRepository, QueryFailedError } from "typeorm";
import { User } from "src/users/user.entity";
import { AuthCredentialsDTO } from "./dto/auth-credentials.dto";
import { InternalServerErrorException, ConflictException } from "@nestjs/common";
import { AuthErrorType } from "./auth-error.enum";

@EntityRepository(User)
export class AuthRepository extends Repository<User> {
  async register(authCredentialsDTO: AuthCredentialsDTO): Promise<void> {
    const { email, password } = authCredentialsDTO;

    const user = new User();
    user.email = email;
    user.password = password;

    try {
      await user.save();
    } catch (error) {
      
      if (error.code === AuthErrorType.DUPLICATE_EMAIL) {
        throw new ConflictException(`Email was existsed before.`);
      }

      throw new InternalServerErrorException();
    }
  }
}