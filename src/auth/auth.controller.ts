import { Controller, Get, Post, Body, ValidationPipe } from '@nestjs/common';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {

  constructor(private authService : AuthService) {}

  @Post("/signup")
  signup(@Body(ValidationPipe) authCredentialsDTO : AuthCredentialsDTO) : Promise<void> {
    return this.authService.register(authCredentialsDTO);
  }


}
