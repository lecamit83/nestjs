import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { AuthRepository } from './auth.repository';

@Injectable()
export class AuthService {
  constructor(@InjectRepository(AuthRepository) private authRepository : AuthRepository) {}

  async register(authCredentialsDTO : AuthCredentialsDTO) : Promise<void> {
    return this.authRepository.register(authCredentialsDTO);
  }
}
