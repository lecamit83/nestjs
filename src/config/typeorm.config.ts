import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const typeORMConfig : TypeOrmModuleOptions = {
  type : 'postgres',
  host : "localhost",
  port : 5432,
  username : "postgres",
  password : "lecam16T1",
  database : "taskmanagement",
  entities : ["../**/*.entity.ts"],
  synchronize : true,
  autoLoadEntities: true,
}